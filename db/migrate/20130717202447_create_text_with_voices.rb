class CreateTextWithVoices < ActiveRecord::Migration
  def change
    create_table :text_with_voices do |t|
      t.string :title
      t.string :author
      t.string :category
      t.string :language

      t.timestamps
    end
  end
end
