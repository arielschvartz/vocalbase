class CreateUploadedFiles < ActiveRecord::Migration
  def change
    create_table :uploaded_files do |t|
      t.integer :user_id
      t.string :url
      t.string :format
      t.integer :sum_votes
      t.integer :num_votes
      t.references :owner_file, polymorphic: true

      t.timestamps
    end

    add_index :uploaded_files, :user_id
  end
end
