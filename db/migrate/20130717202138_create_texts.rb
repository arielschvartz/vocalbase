class CreateTexts < ActiveRecord::Migration
  def change
    create_table :texts do |t|
      t.string :title
      t.string :author
      t.string :category
      t.string :language

      t.timestamps
    end
  end
end
