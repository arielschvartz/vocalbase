class CreateAudioWords < ActiveRecord::Migration
  def change
    create_table :audio_words do |t|
      t.string :word
      t.boolean :gender
      t.string :language
      t.string :country
      t.string :state
      t.string :sampling_freq

      t.timestamps
    end
  end
end
