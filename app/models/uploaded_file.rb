class UploadedFile < ActiveRecord::Base
  attr_accessible :url, :format

  belongs_to :user
  belongs_to :owner_file, polymorphic: true

  validates_presence_of :url

  before_save :set_format

  private

  	def set_format
  		if format.blank?
  			format = url.split(".").last
  		end
  	end
end