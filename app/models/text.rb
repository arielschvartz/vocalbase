class Text < ActiveRecord::Base
  attr_accessible :author, :category, :language, :title

  has_one :uploaded_file, as: :owner_file

  # validates_presence_of :author, :category, :language, :title

  def file_path
  	unless uploaded_file.nil?
	  	uploaded_file.url
  	end
  end

end
