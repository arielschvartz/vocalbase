class AudioWord < ActiveRecord::Base
  attr_accessible :country, :gender, :language, :sampling_freq, :state, :word

  has_one :uploaded_file, as: :owner_file

  # validates_presence_of :country, :gender, :language, :state, :word

  def file_path
  	unless uploaded_file.nil?
	  	uploaded_file.url
  	end
  end

  def title
  	word
  end
end
