class TextWithVoice < ActiveRecord::Base
  attr_accessible :author, :category, :language, :title

  has_many :uploaded_files, as: :owner_file

  # validates_presence_of :author, :category, :language, :title

  def file_path
  	if voice_path.nil?
  		return text_path
  	else
  		return voice_path
  	end
  end

  def text_path
  	uploaded_files.each do |uf|
  		unless uf.format.in?["wma", "mp3", "ogg", "wav"]
  			return uf.url
  		end
  	end
  	return nil
  end

  def voice_path
  	uploaded_files.each do |uf|
  		if uf.format.in?["wma", "mp3", "ogg", "wav"]
  			return uf.url
  		end
  	end
  	return nil
  end
end
