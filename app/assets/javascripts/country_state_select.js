$(document).ready(function() {
	$("select.country_select").change(function(event){
		select_wrapper = $('.state_wrapper');
		$('select', select_wrapper).attr('disabled', true);
		country_code = $(this).val().replace(/ /g, "_");
		type = $("#country_select_type").val();
		url = "/state_select?parent_region="+country_code+"&type="+type;
		select_wrapper.load(url);
	})
});