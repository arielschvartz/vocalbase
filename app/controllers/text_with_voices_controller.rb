# -*- encoding : utf-8 -*-

class TextWithVoicesController < ApplicationController
  before_filter :authenticate_user!, only: [:new, :create]
  
  def show
  end

  def new
    @text_with_voice = TextWithVoice.new
  end

  def create
    text_url = params[:text_with_voice].delete(:text_path)
    voice_url = params[:text_with_voice].delete(:voice_path)

    @text_with_voice = TextWithVoice.new params[:text_with_voice]
    if @text_with_voice.save
      @text_file = @text_with_voice.uploaded_files.new
      @text_file.user = current_user
      @text_file.url = text_url
      @text_file.format = params.delete(:text_format)
      if @text_file.save
        @voice_file = @text_with_voice.uploaded_files.new
        @voice_file.user = current_user
        @voice_file.url = voice_url
        @voice_file.format = params.delete(:voice_format)
        if @voice_file.save
          flash[:success] = "Cadastro de texto com voz efetuado com sucesso."
          redirect_to root_path
        else
          @text_with_voice.destroy
          @text_file.destroy
          flash[:error] = "Por favor complete todos os campos obrigatórios corretamente."
          render :new
        end
      else
        @text_with_voice.destroy
        flash[:error] = "Por favor complete todos os campos obrigatórios corretamente."
        render :new
      end
    else
      flash[:error] = "Por favor complete todos os campos obrigatórios corretamente."
      render :new
    end
  end

  def edit
  end

  def update
  end
end
