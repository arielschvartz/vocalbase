# -*- encoding : utf-8 -*-

class TextsController < ApplicationController
  before_filter :authenticate_user!, only: [:new, :create]
  
  def show
  end

  def new
    @text = Text.new
  end

  def create
    url = params[:text].delete(:text_path)
    @text = Text.new params[:text]
    if @text.save
      @text_file = UploadedFile.new
      @text_file.owner_file = @text
      @text_file.user = current_user
      @text_file.url = url
      @text_file.format = params.delete(:text_format)
      if @text_file.save
        flash[:success] = "Cadastro de texto efetuado com sucesso."
        redirect_to root_path
      else
        @text.destroy
        flash[:error] = "Por favor complete todos os campos obrigatórios corretamente."
        render :new
      end
    else
      flash[:error] = "Por favor complete todos os campos obrigatórios corretamente."
      render :new
    end
  end

  def edit
  end

  def update
  end
end
