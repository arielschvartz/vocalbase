class UploadedFilesController < ApplicationController
  def index
  	@all_words_raw = AudioWord.all
  	@all_words = search [:word, :language, :country, :state, :sampling_freq], @all_words_raw

  	@all_texts_raw = Text.all
  	@all_texts = search [:title, :author, :category, :language], @all_texts_raw

  	@all_texts_with_voices_raw = TextWithVoice.all
  	@all_texts_with_voices = search [:title, :author, :category, :language], @all_texts_with_voices_raw

  	if params[:Filtros] == "Palavra"
  		@all = @all_words
  	elsif params[:Filtros] == "Texto com Voz"
  		@all = @all_texts_with_voices
  	elsif params[:Filtros] == "Texto apenas"
  		@all = @all_texts
  	else
  		@all = (@all_words << @all_texts << @all_texts_with_voices).flatten
  	end
  end

  def show
    @me = UploadedFile.find params[:id]
  end
  
  def new
  	@uploaded_file = UploadedFile.new
  end

  private

  	def search attributes, raw
  		new_array = []
  		params[:search].split(" ").each do |word|
	  		attributes.each do |attr|
	  			new_raw = raw.dup
	  			new_raw.each do |rec|
	  				# if attr == :gender
	  				# 	if rec.gender == true && "masculino".include?(word.downcase)
							# @all_words << rec
			  		# 		@all_words_raw.delete(rec)
			  		# 		break
			  		# 	elsif rec.gender == false && "feminino".include?(word.downcase)
			  		# 		@all_words << rec
			  		# 		@all_words_raw.delete(rec)
			  		# 		break
	  				# 	end
	  				# end
	  				if !rec[attr].nil? && rec[attr].downcase.include?(word.downcase)
	  					new_array << rec
	  					raw.delete(rec)
	  				end
	  			end
	  		end
	  	end
	  	new_array
  	end
end
