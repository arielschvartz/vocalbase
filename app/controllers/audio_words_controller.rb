# -*- encoding : utf-8 -*-

class AudioWordsController < ApplicationController
  before_filter :authenticate_user!, only: [:new, :create]
  
  def show
  end

  def new
    @audio_word = AudioWord.new
  end

  def create
    if params[:audio_word][:gender] == "Masculino"
      params[:audio_word][:gender] = true
    else
      params[:audio_word][:gender] = false
    end
    url = params[:audio_word].delete(:file_path)
    @audio_word = AudioWord.new params[:audio_word]
    if @audio_word.save
      @uploaded_file = UploadedFile.new
      @uploaded_file.owner_file = @audio_word
      @uploaded_file.user = current_user
      @uploaded_file.url = url
      @uploaded_file.format = params.delete(:format)
      if @uploaded_file.save
        flash[:success] = "Cadastro de palavra efetuado com sucesso."
        redirect_to root_path
      else
        @audio_word.destroy
        flash[:error] = "Por favor complete todos os campos obrigatórios corretamente."
        render :new
      end
    else
      flash[:error] = "Por favor complete todos os campos obrigatórios corretamente."
      render :new
    end
  end

  def edit
  end

  def update
  end

  def destroy
  end
end
