VocalBase::Application.routes.draw do
  get '/state_select' => 'countries_states#state_select'

  devise_for :users,
    path: '',
    path_names: {
      sign_out: 'logout'
    }

  resources :uploaded_files, only: [:new, :index, :show], path: 'arquivos',
    path_names: {
      :new => 'upload',
      :show => 'arquivo'
    }

  resources :audio_words, only: [:new, :create]
  resources :text_with_voices, only: [:new, :create]
  resources :texts, only: [:new, :create]

  root to: "static_pages#home"
end
